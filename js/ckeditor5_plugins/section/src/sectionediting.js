import { Plugin } from 'ckeditor5/src/core';
import { Enter } from 'ckeditor5/src/enter';
import { Delete } from 'ckeditor5/src/typing';

import SectionCommand from './sectioncommand';

export default class SectionEditing extends Plugin {
  static get pluginName() {
    return 'SectionEditing';
  }

  static get requires() {
    return [ Enter, Delete ];
  }

  init() {
    const editor = this.editor;
    const schema = editor.model.schema;

    editor.commands.add( 'blockSection', new SectionCommand( editor ) );
    editor.model.document.registerPostFixer( writer => {
      const changes = editor.model.document.differ.getChanges();

      for ( const entry of changes ) {
        if ( entry.type == 'insert' ) {
          const element = entry.position.nodeAfter;

          if ( !element ) {
            continue;
          }

          if ( element.is( 'element', 'htmlSection' ) && element.isEmpty ) {
            writer.remove( element );

            return true;
          } else if ( element.is( 'element', 'htmlSection' ) && !schema.checkChild( entry.position, element ) ) {
            writer.unwrap( element );

            return true;
          } else if ( element.is( 'element' ) ) {
            const range = writer.createRangeIn( element );

            for ( const child of range.getItems() ) {
              if (
                child.is( 'element', 'htmlSection' ) &&
                !schema.checkChild( writer.createPositionBefore( child ), child )
              ) {
                writer.unwrap( child );

                return true;
              }
            }
          }
        } else if ( entry.type == 'remove' ) {
          const parent = entry.position.parent;

          if ( parent.is( 'element', 'htmlSection' ) && parent.isEmpty ) {
            writer.remove( parent );

            return true;
          }
        }
      }

      return false;
    } );

    const viewDocument = this.editor.editing.view.document;
    const selection = editor.model.document.selection;
    const blockSectionCommand = editor.commands.get( 'blockSection' );

    this.listenTo( viewDocument, 'enter', ( evt, data ) => {
      if ( !selection.isCollapsed || !blockSectionCommand.value ) {
        return;
      }

      const positionParent = selection.getLastPosition().parent;

      if ( positionParent.isEmpty ) {
        editor.execute( 'blockSection' );
        editor.editing.view.scrollToTheSelection();

        data.preventDefault();
        evt.stop();
      }
    }, { context: 'section' } );

    this.listenTo( viewDocument, 'delete', ( evt, data ) => {
      if ( data.direction != 'backward' || !selection.isCollapsed || !blockSectionCommand.value ) {
        return;
      }

      const positionParent = selection.getLastPosition().parent;

      if ( positionParent.isEmpty && !positionParent.previousSibling ) {
        editor.execute( 'blockSection' );
        editor.editing.view.scrollToTheSelection();

        data.preventDefault();
        evt.stop();
      }
    }, { context: 'section' } );
  }
}
