import { Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';
import icon from '../../../../icons/section.svg';

export default class SectionUI extends Plugin {

  static get pluginName() {
    return 'SectionUI';
  }

  init() {
    const editor = this.editor;
    const t = editor.t;

    editor.ui.componentFactory.add('section', locale => {
      const command = editor.commands.get( 'blockSection' );
      const buttonView = new ButtonView( locale );

      buttonView.set( {
        label: t( 'Section' ),
        icon,
        tooltip: true,
        isToggleable: true
      } );

      buttonView.bind( 'isOn', 'isEnabled' ).to( command, 'value', 'isEnabled' );

      this.listenTo( buttonView, 'execute', () => {
        editor.execute( 'blockSection' );
        editor.editing.view.focus();
      } );

      return buttonView;
    } );
  }
}
