import { Plugin } from 'ckeditor5/src/core';

import SectionEditing from './sectionediting';
import SectionUI from './sectionui';

export default class Section extends Plugin {
  static get requires() {
    return [ SectionEditing, SectionUI ];
  }

  static get pluginName() {
    return 'Section';
  }
}
