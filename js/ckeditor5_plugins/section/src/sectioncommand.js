import { Command } from 'ckeditor5/src/core';
import { first } from 'ckeditor5/src/utils';

export default class SectionCommand extends Command {
  refresh() {
    this.value = this._getValue();
    this.isEnabled = this._checkEnabled();
  }

  execute( options = {} ) {
    const model = this.editor.model;
    const schema = model.schema;
    const selection = model.document.selection;

    const blocks = Array.from( selection.getSelectedBlocks() );

    const value = ( options.forceValue === undefined ) ? !this.value : options.forceValue;

    model.change( writer => {
      if ( !value ) {
        this._removeSection( writer, blocks.filter( findSection ) );
      } else {
        const blocksToSection = blocks.filter( block => {
          return findSection( block ) || checkCanBeSectioned( schema, block );
        } );

        this._applySection( writer, blocksToSection );
      }
    } );
  }

  _getValue() {
    const selection = this.editor.model.document.selection;

    const firstBlock = first( selection.getSelectedBlocks() );

    return !!( firstBlock && findSection( firstBlock ) );
  }

  _checkEnabled() {
    if ( this.value ) {
      return true;
    }

    const selection = this.editor.model.document.selection;
    const schema = this.editor.model.schema;

    const firstBlock = first( selection.getSelectedBlocks() );

    if ( !firstBlock ) {
      return false;
    }

    return checkCanBeSectioned( schema, firstBlock );
  }

  _removeSection( writer, blocks ) {
    getRangesOfBlockGroups( writer, blocks ).reverse().forEach( groupRange => {
      if ( groupRange.start.isAtStart && groupRange.end.isAtEnd ) {
        writer.unwrap( groupRange.start.parent );

        return;
      }

      // The group of blocks are at the beginning of an <bQ> so let's move them left (out of the <bQ>).
      if ( groupRange.start.isAtStart ) {
        const positionBefore = writer.createPositionBefore( groupRange.start.parent );

        writer.move( groupRange, positionBefore );

        return;
      }

      // The blocks are in the middle of an <bQ> so we need to split the <bQ> after the last block
      // so we move the items there.
      if ( !groupRange.end.isAtEnd ) {
        writer.split( groupRange.end );
      }

      // Now we are sure that groupRange.end.isAtEnd is true, so let's move the blocks right.

      const positionAfter = writer.createPositionAfter( groupRange.end.parent );

      writer.move( groupRange, positionAfter );
    } );
  }

  _applySection( writer, blocks ) {
    const sectionsToMerge = [];

    // Section all groups of block. Iterate in the reverse order to not break following ranges.
    getRangesOfBlockGroups( writer, blocks ).reverse().forEach( groupRange => {
      let section = findSection( groupRange.start );

      if ( !section ) {
        section = writer.createElement( 'htmlSection' );

        writer.wrap( groupRange, section );
      }

      sectionsToMerge.push( section );
    } );

    sectionsToMerge.reverse().reduce( ( currentSection, nextSection ) => {
      if ( currentSection.nextSibling == nextSection ) {
        writer.merge( writer.createPositionAfter( currentSection ) );

        return currentSection;
      }

      return nextSection;
    } );
  }
}

function findSection( elementOrPosition ) {
  return elementOrPosition.parent.name == 'htmlSection' ? elementOrPosition.parent : null;
}

function getRangesOfBlockGroups( writer, blocks ) {
  let startPosition;
  let i = 0;
  const ranges = [];

  while ( i < blocks.length ) {
    const block = blocks[ i ];
    const nextBlock = blocks[ i + 1 ];

    if ( !startPosition ) {
      startPosition = writer.createPositionBefore( block );
    }

    if ( !nextBlock || block.nextSibling != nextBlock ) {
      ranges.push( writer.createRange( startPosition, writer.createPositionAfter( block ) ) );
      startPosition = null;
    }

    i++;
  }

  return ranges;
}

function checkCanBeSectioned( schema, block ) {
  // TMP will be replaced with schema.checkWrap().
  const isSectionAllowed = schema.checkChild( block.parent, 'htmlSection' );
  const isBlockAllowedInSection = schema.checkChild( [ '$root', 'htmlSection' ], block );

  return isSectionAllowed && isBlockAllowedInSection
}
